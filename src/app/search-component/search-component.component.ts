import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddNewFormComponent } from '../add-new-form/add-new-form.component';

@Component({
  selector: 'app-search-component',
  templateUrl: './search-component.component.html',
  styleUrls: ['./search-component.component.scss'],
})
export class SearchComponentComponent implements OnInit {

  @ViewChild("expandWrapper", { read: ElementRef }) expandWrapper: ElementRef;
  @Input("expanded") expanded: boolean = false;
  @Input("expandHeight") expandHeight: string = "100%";

  buttonList:any[]=[
    {
      icon:'add-outline',
      bgColor:'#e1e1e1',
      color:"black",
      name:"add"
    },
    {
      icon:'cloud-download-outline',
      bgColor:'#e1e1e1',
      color:"black",
      name:"download"
    },
    {
      icon:'cloud-upload-sharp',
      bgColor:'#c9d8e5',
      color:"#5040c1",
      name:"upload"
    },
    
   
  ]
  constructor(public renderer: Renderer2,private modalController:ModalController) {}

  ngOnInit() {}

  onClickItem(name:string){

    if(name=="add"){
      this.presentModal();
    }

  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddNewFormComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'firstName': 'Douglas',
        'lastName': 'Adams',
        'middleInitial': 'N'
      }
    });
    return await modal.present();
  }

}
